import {commandMetadata} from "modules/commandMetadata.js";

const metadata: commandMetadata = {
        name: "hello",
        description: "test thing",
        usage: ""
};

export function run(client: any, roomId: string, event: any): void {
        client.replyNotice(roomId, event, "Hello world!");
};

export { metadata };
