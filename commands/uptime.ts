import {commandMetadata} from "modules/commandMetadata.js";

const metadata: commandMetadata = {
        name: "uptime",
        description: "Gets the current uptime of the bot",
        usage: ""
};

export function run(client: any, roomId: string): void {
        // https://gitlab.com/procps-ng/procps/-/blob/master/library/uptime.c
        const SECONDS_IN_MINUTE = 60;
        const SECONDS_IN_HOUR = 3600;
        const SECONDS_IN_DAY = 86400;
        const SECONDS_IN_WEEK = 604800;
        const SECONDS_IN_YEAR = 31536000;

        const totalUptimeSeconds: number = Math.floor(process.uptime());
        let uptimeSeconds: number = totalUptimeSeconds;
        let uptimeMinutes: number = 0;
        let uptimeHours: number = 0;
        let uptimeDays: number = 0;
        let uptimeWeeks: number = 0;
        let uptimeYears: number = 0;

        let commaState: number = 0;
        let formattedString: string = '';

        function formatUptime(value: number, unit: string, isMinute?: boolean): void {
                const separator = commaState ? ", " : "";
                const timeUnit = value > 1 ? `${unit}s` : unit;

                if (isMinute && totalUptimeSeconds < 60) {
                        formattedString += `${separator}${value} minutes`;
                        commaState += 1;
                } else if (value) {
                        formattedString += `${separator}${value} ${timeUnit}`;
                        commaState += 1;
                }

        }

        if (uptimeSeconds >= SECONDS_IN_YEAR) {
                uptimeYears = Math.floor(uptimeSeconds / SECONDS_IN_YEAR);
                uptimeSeconds -= uptimeYears * SECONDS_IN_YEAR;
        }

        if (uptimeSeconds >= SECONDS_IN_WEEK) {
                uptimeWeeks = Math.floor(uptimeSeconds / SECONDS_IN_WEEK);
                uptimeSeconds -= uptimeWeeks * SECONDS_IN_WEEK;
        }

        if (uptimeSeconds >= SECONDS_IN_DAY) {
                uptimeDays = Math.floor(uptimeSeconds / SECONDS_IN_DAY);
                uptimeSeconds -= uptimeDays * SECONDS_IN_DAY;
        }

        if (uptimeSeconds >= SECONDS_IN_HOUR) {
                uptimeHours = Math.floor(uptimeSeconds / SECONDS_IN_HOUR);
                uptimeSeconds -= uptimeHours * SECONDS_IN_HOUR;
        }

        if (uptimeSeconds >= SECONDS_IN_MINUTE) {
                uptimeMinutes = Math.floor(uptimeSeconds / SECONDS_IN_MINUTE);
                uptimeSeconds -= uptimeMinutes * SECONDS_IN_MINUTE;
        }

        formatUptime(uptimeYears, "year");
        formatUptime(uptimeWeeks, "week");
        formatUptime(uptimeDays, "day");
        formatUptime(uptimeHours, "hour");
        formatUptime(uptimeMinutes, "minute", true);

        client.sendHtmlNotice(roomId, `Uptime: <code>${formattedString}</code>`);
};

export { metadata };
