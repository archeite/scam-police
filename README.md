# Scam Police

A Matrix bot that warns about scams posted in public rooms.

## Setup

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run index.ts
```

This project was created using `bun init` in bun v0.5.9. [Bun](https://bun.sh)
is a fast all-in-one JavaScript runtime.

## License

Licensed under the
[AGPLv3](https://codeberg.org/archeite/scam-police/src/branch/main/LICENSE)
license.
