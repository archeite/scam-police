export interface commandMetadata {
        name: string;
        description: string;
        usage: string;
}
