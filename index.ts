/**
 * scam-police v0.1.0
 * https://codeberg.org/archeite/scam-police
 **/

import { AutojoinRoomsMixin, MatrixClient, RustSdkCryptoStorageProvider, SimpleFsStorageProvider } from "matrix-bot-sdk";
import { parse } from "yaml";
import * as dotenv from 'dotenv';
import { readdirSync, readFileSync} from "fs";

const configurationFile = readFileSync('./configuration/config.yaml', 'utf8');
dotenv.config();

const configuration = {
        accessToken: process.env.ACCESS_TOKEN || '',
        homeserver: process.env.HOMESERVER || '',
        commandPrefix: parse(configurationFile)["commandPrefix"] || '',
        commands: new Map()
};

const storage = new SimpleFsStorageProvider("./database/bot-state.json");
const cryptoStore = new RustSdkCryptoStorageProvider("./database/crypto-storage/");
const client = new MatrixClient(configuration.homeserver, configuration.accessToken, storage, cryptoStore);

try {
        const commands = readdirSync("./commands").filter(file => file.endsWith(".ts"));
        for (let file of commands) {
                const commandName = file.split(".")[0];
                const command = await import(`./commands/${commandName}.ts`);
                configuration.commands.set(commandName, command);
        };
} catch (error: any) {
        console.error(`Error loading / importing commands: ${error.message}`);
};

AutojoinRoomsMixin.setupOnClient(client);

client.start().then(() => {
        client.setPresenceStatus("online");
        console.log("Bot Started!");
});

// Before we start the bot, register our command handler
client.on("room.message", handleCommand);

// This is the command handler we registered a few lines up
async function handleCommand(roomId: string, event: any) {
    // Don't handle unhelpful events (ones that aren't text messages, are redacted, or sent by us)
    if (event['content']?.['msgtype'] !== 'm.text') return;
    if (event['sender'] === await client.getUserId()) return;
    
    // Check to ensure that the `!hello` command is being run
    const body = event['content']['body'];
    if (!body?.startsWith(configuration.commandPrefix)) return;

    const commandArguments = body.slice(configuration.commandPrefix.length).trim().split(/ +/g);
    const commandName = commandArguments.shift().toLowerCase();
    const command = configuration.commands.get(commandName);
    if (!command) return;

    try {
            await client.sendReadReceipt(roomId, event["event_id"]);
            await command.run(client, roomId, event, commandArguments);
    } catch (error: any) {
            console.error(`Error running the '${commandName}' command: ${error.message}`)
    }
}
